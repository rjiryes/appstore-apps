//
//  App+CoreDataProperties.h
//  AppstoreApps
//
//  Created by Ramzi Jiryes on 1/19/16.
//  Copyright © 2016 rj. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "App.h"

NS_ASSUME_NONNULL_BEGIN

@interface App (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *appDescription;
@property (nullable, nonatomic, retain) NSString *category;
@property (nullable, nonatomic, retain) NSString *creatorName;
@property (nullable, nonatomic, retain) NSString *creatorUrl;
@property (nullable, nonatomic, retain) NSString *imageUrl;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *price;
@property (nullable, nonatomic, retain) NSString *currency;
@property (nullable, nonatomic, retain) NSString *thumbUrl;
@property (nullable, nonatomic, retain) NSString *itunesUrl;
@property (nullable, nonatomic, retain) NSString *releaseDate;

@end

NS_ASSUME_NONNULL_END
