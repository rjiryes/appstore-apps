//
//  DataController.h
//  AppstoreApps
//
//  Created by Ramzi Jiryes on 1/19/16.
//  Copyright © 2016 rj. All rights reserved.
//


#import <Foundation/Foundation.h>
@class DataController;
@protocol DataDelegate <NSObject>

@optional
-(void)didFetchAppsFromLocal:(NSArray *)apps;
-(void)didDeleteAllAppsFromLocal;

@end

@interface DataController : NSObject
@property (nonatomic, assign) id delegate;
-(void)fetchAppsFromLocal;
- (void)deleteAllAppsFromLocal;
@end
