//
//  AppDetailTableViewController.m
//  AppstoreApps
//
//  Created by Ramzi Jiryes on 1/19/16.
//  Copyright © 2016 rj. All rights reserved.
//

#import "AppDetailTableViewController.h"
#import <UIImageView+AFNetworking.h>
@interface AppDetailTableViewController ()

@end

@implementation AppDetailTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView setEstimatedRowHeight:100];
    
    [self.nameLabel setText:[_app name]];
    [self.descriptionLabel setText:[_app appDescription]];
    [self.appImageView setImageWithURL:[NSURL URLWithString:[_app imageUrl]] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    [self.priceLabel setText:[NSString stringWithFormat:@"%.2f",[[_app price] floatValue]]]
    ;
    [self.currencyLabel setText:[_app currency]];
    [self.categoryLabel setText:[_app category]];
    [self.releaseDateLabel setText:[_app releaseDate]];
    [self.creatorLabel setText:[_app creatorName]];
    

    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 1){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_app creatorUrl]]];
    }else if(indexPath.row == 2){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_app itunesUrl]]];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end