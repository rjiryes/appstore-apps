//
//  AppsCollectionViewController.h
//  AppstoreApps
//
//  Created by Ramzi Jiryes on 1/19/16.
//  Copyright © 2016 rj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataController.h"

@interface AppsCollectionViewController : UICollectionViewController <DataDelegate,UICollectionViewDelegateFlowLayout>

@end
