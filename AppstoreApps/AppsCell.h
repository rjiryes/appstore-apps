//
//  AppsCell.h
//  AppstoreApps
//
//  Created by Ramzi Jiryes on 1/19/16.
//  Copyright © 2016 rj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppsCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *appImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;

@end
