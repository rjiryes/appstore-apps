//
//  main.m
//  AppstoreApps
//
//  Created by Ramzi Jiryes on 1/19/16.
//  Copyright © 2016 rj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
