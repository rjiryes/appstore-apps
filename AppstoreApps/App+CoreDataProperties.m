//
//  App+CoreDataProperties.m
//  AppstoreApps
//
//  Created by Ramzi Jiryes on 1/19/16.
//  Copyright © 2016 rj. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "App+CoreDataProperties.h"

@implementation App (CoreDataProperties)

@dynamic appDescription;
@dynamic category;
@dynamic creatorName;
@dynamic creatorUrl;
@dynamic imageUrl;
@dynamic name;
@dynamic price;
@dynamic currency;
@dynamic thumbUrl;
@dynamic itunesUrl;
@dynamic releaseDate;

@end
