//
//  AppDetailTableViewController.h
//  AppstoreApps
//
//  Created by Ramzi Jiryes on 1/19/16.
//  Copyright © 2016 rj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "App.h"
@interface AppDetailTableViewController : UITableViewController
@property (strong, nonatomic) App *app;

@property (strong, nonatomic) IBOutlet UIImageView *appImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UILabel *releaseDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *creatorLabel;
@property (strong, nonatomic) IBOutlet UILabel *categoryLabel;
@property (strong, nonatomic) IBOutlet UILabel *currencyLabel;

@end
