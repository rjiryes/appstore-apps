//
//  DataController.m
//  AppstoreApps
//
//  Created by Ramzi Jiryes on 1/19/16.
//  Copyright © 2016 rj. All rights reserved.
//


#import "DataController.h"
#import "AppDelegate.h"
#import <AFNetworking.h>
#import <AFHTTPSessionManager.h>
#import "App.h"

@implementation DataController{
    AppDelegate *appDelegate;
}

-(id)init{
    
    if (self=[super init])
    {
        appDelegate = [[UIApplication sharedApplication] delegate];
    }
    return self;
}



-(void)fetchDataFromServer{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:@"https://itunes.apple.com/us/rss/topgrossingapplications/limit=50/json" parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        [self createAppObjectsFromResponse:responseObject];
        [self fetchAppsFromLocal];
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
}


-(void)createAppObjectsFromResponse:(id)responseObject{
    
    NSArray *arrayOfApps = [[responseObject valueForKey:@"feed"] valueForKey:@"entry"];
    for(int i=0;i<arrayOfApps.count;i++){
        
        //Create App object
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"App" inManagedObjectContext:appDelegate.managedObjectContext];
        App *newApp  = [[App alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:appDelegate.managedObjectContext];
        
        [newApp setName:[[[arrayOfApps objectAtIndex:i] valueForKey:@"im:name"] valueForKey:@"label"]];
        [newApp setAppDescription:[[[arrayOfApps objectAtIndex:i] valueForKey:@"summary"]  valueForKey:@"label"]];
        [newApp setThumbUrl:[[[[arrayOfApps objectAtIndex:i] valueForKey:@"im:image"] objectAtIndex:0]  valueForKey:@"label"]];
        [newApp setImageUrl:[[[[arrayOfApps objectAtIndex:i] valueForKey:@"im:image"] objectAtIndex:2]  valueForKey:@"label"]];
        [newApp setPrice:[[[[arrayOfApps objectAtIndex:i] valueForKey:@"im:price"]  valueForKey:@"attributes"] valueForKey:@"amount"]];
        [newApp setCurrency:[[[[arrayOfApps objectAtIndex:i] valueForKey:@"im:price"]  valueForKey:@"attributes"] valueForKey:@"currency"]];

        
        [newApp setCategory:[[[[arrayOfApps objectAtIndex:i] valueForKey:@"category"]  valueForKey:@"attributes"] valueForKey:@"label"]];
        [newApp setCreatorName:[[[arrayOfApps objectAtIndex:i] valueForKey:@"im:artist"] valueForKey:@"label"]];
        [newApp setCreatorUrl:[[[[arrayOfApps objectAtIndex:i] valueForKey:@"im:artist"] valueForKey:@"attributes"] valueForKey:@"href"]];
        [newApp setItunesUrl:[[[arrayOfApps objectAtIndex:i] valueForKey:@"id"] valueForKey:@"label"]];
        [newApp setReleaseDate:[[[[arrayOfApps objectAtIndex:i] valueForKey:@"im:releaseDate"]  valueForKey:@"attributes"] valueForKey:@"label"]];
        //Save
        NSError *error = nil;
        if (![newApp.managedObjectContext save:&error]) {
            NSLog(@"Unable to save managed object context.");
            NSLog(@"%@, %@", error, error.localizedDescription);
        }
        
        
    }
}


- (void)deleteAllAppsFromLocal{
    NSString *nameEntity = @"App";
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:nameEntity];
    [fetchRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error;
    NSArray *fetchedObjects = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject *object in fetchedObjects)
    {
        [appDelegate.managedObjectContext deleteObject:object];
    }
    
    error = nil;
    if (![appDelegate.managedObjectContext save:&error]) {
        NSLog(@"Unable to save managed object context.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }else{
        [self.delegate didDeleteAllAppsFromLocal];
    }
    
}

-(void)fetchAppsFromLocal{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"App" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error){
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    }else{
        if(result.count == 0){
            [self fetchDataFromServer];
        }else{
            [self.delegate didFetchAppsFromLocal:result];
        }
        
    }
}

@end
