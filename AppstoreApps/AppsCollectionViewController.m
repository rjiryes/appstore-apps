//
//  AppsCollectionViewController.m
//  AppstoreApps
//
//  Created by Ramzi Jiryes on 1/19/16.
//  Copyright © 2016 rj. All rights reserved.
//

#import "AppsCollectionViewController.h"
#import "AppDetailTableViewController.h"
#import "AppsCell.h"
#import <UIImageView+AFNetworking.h>
#import "App.h"
#import <SVProgressHud.h>
@interface AppsCollectionViewController ()
@property (nonatomic, strong) NSMutableArray *applications;
@property (strong, nonatomic) DataController *dataController;

@end

@implementation AppsCollectionViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    _applications = [[NSMutableArray alloc] init];
    _dataController = [[DataController alloc] init];
    
    [_dataController setDelegate:self];
    [SVProgressHUD show];
    [_dataController fetchAppsFromLocal];
    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshData)];
    [self.navigationItem setRightBarButtonItem:refreshButton];
}

-(void)refreshData{
    [SVProgressHUD show];
    [_dataController deleteAllAppsFromLocal];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _applications.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    AppsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"appsCell" forIndexPath:indexPath];
    App *app = [_applications objectAtIndex:indexPath.row];
    
    [[cell appImageView] setImageWithURL:[NSURL URLWithString:[app imageUrl]] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    [[cell nameLabel]setText:[app name]];
    
    // Configure the cell
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    AppDetailTableViewController *appDetailVc = (AppDetailTableViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"appDetailVc"];
    [appDetailVc setApp:[_applications objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:appDetailVc animated:YES];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((self.collectionView.frame.size.width/3)-1, (self.collectionView.frame.size.width/3)-1);
}

#pragma mark - Data Controller Delegate

-(void)didFetchAppsFromLocal:(NSArray *)apps{
    _applications = [NSMutableArray arrayWithArray:apps];
    [SVProgressHUD dismiss];
    [self.collectionView reloadData];
}

-(void)didDeleteAllAppsFromLocal{
    [_dataController fetchAppsFromLocal];
}

@end
